/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package DAO;

/**
 * Clase que se utiliza para implementar el patron DAO
 * @author gabriela Delgado
 * Version: 1.0
 */
public interface DAOLogin {
    
    /**
     * Verifica si el usuario y contraseña son validos
     * @param usuario Nombre
     * @param contrasenia Contraseña
     * @return En caso de que sea correcto el estado es igual a TRUE
     */
    public boolean VerificarUsuarios(String usuario, String contrasenia);
    
    /**
     * Devuelve el nombre del usuario
     * @return El nombre del usuario
     */
    public String getUsuario();
    
    /**
     * Metodo para cambiar contraseña
     * @param contrasenia
     * @return Contraseña cambiada
     */
    public boolean CambiarContrasenia(String contrasenia);
    
}

